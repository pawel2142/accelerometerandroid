package eti.asw.accelerometer.accelerometer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Observable;
import java.util.Observer;

public class DisplacementActivity extends AppCompatActivity implements Observer {

    private AccelerometerData accelerometerData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_displacement);

        accelerometerData = new AccelerometerData(this);
        accelerometerData.addObserver(this);
    }

    private void refreshDisplacementTextView(){
        TextView velocityXAxis = (TextView) findViewById(R.id.displacementXAxis);
        TextView velocityYAxis = (TextView) findViewById(R.id.displacementYAxis);
        TextView velocityZAxis = (TextView) findViewById(R.id.displacementZAxis);

        double[] data = accelerometerData.getLastDisplacement();
        double xAxis = data[0];
        double yAxis = data[1];
        double zAxis = data[2];

        velocityXAxis.setText("x axis[m]: " + String.format("%f", xAxis));
        velocityYAxis.setText("y axis[m]: " + String.format("%f", yAxis));
        velocityZAxis.setText("z axis[m]: " + String.format("%f", zAxis));
    }

    public void onReturnButton(View view){
        finish();
        accelerometerData.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        accelerometerData.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        accelerometerData.pause();
    }

    @Override
    public void update(Observable observable, Object o) {
        refreshDisplacementTextView();
    }
}
